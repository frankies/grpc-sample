#!/bin/sh

mkdir -p gen/java gen/python gen/ruby

protoc --python_out=gen/python --grpc_out=gen/python --plugin=protoc-gen-grpc=`which grpc_python_plugin` -Iproto proto/* 
protoc --java_out=gen/java -Iproto proto/* 
protoc --ruby_out=gen/ruby --grpc_out=gen/ruby --plugin=protoc-gen-grpc=`which grpc_ruby_plugin` -Iproto proto/* 
